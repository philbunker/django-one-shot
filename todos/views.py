from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todos_lists = TodoList.objects.all()
    context = {
        "todos_lists": todos_lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list_name = TodoList.objects.get(id=id)
    todo_list_items = TodoItem.objects.filter(list=id)
    context = {
        "todo_list_id": id,
        "todo_list_name": todo_list_name,
        "todo_list_items": todo_list_items,
    }
    return render(request, "todos/todo_list_detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html", {'id': id})


def create_item(request):
    form = TodoItemForm(request.POST)
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            return redirect("todo_list_detail", id=todo.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
